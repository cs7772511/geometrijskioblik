﻿namespace Apstrakcija
{
    internal interface IGeometrijskiOblik
    {
        string Opis();
    }
}
