﻿namespace Apstrakcija
{
    internal class Pravokutnik : GeometrijskiOblik, IGeometrijskiOblik
    {
        private double duljinaStranice2;
        public Pravokutnik(double duljinaStranice, double duljinaStranice2) : base(duljinaStranice)
        {
            DuljinaStranice2 = duljinaStranice2;
        }
        public double DuljinaStranice2
        {
            get { return duljinaStranice2; }
            set
            {
                if (value >= 0)
                {
                    duljinaStranice2 = value;
                }
                else
                {
                    throw new ArgumentException("Duljina stranice mora biti veca ili jednaka nuli");
                }
            }
        }
        public override double IzracunajPovrsinu()
        {
            return DuljinaStranice*DuljinaStranice2;
        }
        public override string Opis() { return $"{GetType().Name} ({DuljinaStranice}, {DuljinaStranice2}): Povrsina {IzracunajPovrsinu()}"; }

    }
}
