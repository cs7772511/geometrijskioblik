namespace Apstrakcija
{
    public partial class gbKruznica : Form
    {
        Oblici oblici;
        public gbKruznica()
        {
            InitializeComponent();
            oblici = new Oblici();
        }
        private void RefreshDisplay()
        {
            tbPopis.Clear();
            foreach (var oblik in oblici.getOblici())
            {
                tbPopis.AppendText(oblik.Opis() + Environment.NewLine);
            }
        }
        private void btnDodajKvadrat_Click(object sender, EventArgs e)
        {
            double str = double.Parse(tbKvadratStr.Text);
            oblici.dodajOblik(new Kvadrat(str));
            RefreshDisplay();
        }

        private void btnDodajKruznicu_Click(object sender, EventArgs e)
        {
            double str = double.Parse(tbKruznicaR.Text);
            oblici.dodajOblik(new Kruznica(str));
            RefreshDisplay();
        }

        private void btnDodajPravokutnik_Click(object sender, EventArgs e)
        {
            double str1 = double.Parse(tbPravokutnikStr1.Text);
            double str2 = double.Parse(tbPravokutnikStr2.Text);
            oblici.dodajOblik(new Pravokutnik(str1, str2));
            RefreshDisplay();
        }
    }
}