﻿namespace Apstrakcija
{
    internal abstract class GeometrijskiOblik
    {
        private double duljinaStranice;
        public GeometrijskiOblik(double duljinaStranice)
        {
            DuljinaStranice = duljinaStranice;
        }
        public double DuljinaStranice
        {
            get { return duljinaStranice; }
            set
            {
                if (value >= 0)
                {
                    duljinaStranice = value;
                }
                else
                {
                    throw new ArgumentException("Duljina stranice mora biti veca ili jednaka nuli");
                }
            }
        }
        public abstract double IzracunajPovrsinu();
        public virtual string Opis() { return $"{GetType().Name} ({duljinaStranice}): Povrsina {IzracunajPovrsinu()}"; }
    }
}
