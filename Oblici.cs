﻿namespace Apstrakcija
{
    internal class Oblici
    {
        private List<IGeometrijskiOblik> oblici;

        public Oblici()
        {
            oblici = new List<IGeometrijskiOblik>();
        }
        public void dodajOblik(IGeometrijskiOblik oblik)
        {
            oblici.Add(oblik);
        }
        public List<IGeometrijskiOblik> getOblici()
        {
            return oblici;
        }
    }
}
