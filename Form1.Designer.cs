﻿namespace Apstrakcija
{
    partial class gbKruznica
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            gbKvadrat=new GroupBox();
            btnDodajKvadrat=new Button();
            lblStranica=new Label();
            tbKvadratStr=new TextBox();
            groupBox1=new GroupBox();
            btnDodajKruznicu=new Button();
            lblPolumjer=new Label();
            tbKruznicaR=new TextBox();
            gbPravokutnik=new GroupBox();
            lblStranica2=new Label();
            tbPravokutnikStr2=new TextBox();
            btnDodajPravokutnik=new Button();
            lblStranica1=new Label();
            tbPravokutnikStr1=new TextBox();
            tbPopis=new TextBox();
            gbKvadrat.SuspendLayout();
            groupBox1.SuspendLayout();
            gbPravokutnik.SuspendLayout();
            SuspendLayout();
            // 
            // gbKvadrat
            // 
            gbKvadrat.Controls.Add(btnDodajKvadrat);
            gbKvadrat.Controls.Add(lblStranica);
            gbKvadrat.Controls.Add(tbKvadratStr);
            gbKvadrat.Location=new Point(27, 25);
            gbKvadrat.Name="gbKvadrat";
            gbKvadrat.Size=new Size(199, 119);
            gbKvadrat.TabIndex=0;
            gbKvadrat.TabStop=false;
            gbKvadrat.Text="Kvadrat";
            // 
            // btnDodajKvadrat
            // 
            btnDodajKvadrat.Location=new Point(31, 82);
            btnDodajKvadrat.Name="btnDodajKvadrat";
            btnDodajKvadrat.Size=new Size(135, 23);
            btnDodajKvadrat.TabIndex=3;
            btnDodajKvadrat.Text="Dodaj kvadrat";
            btnDodajKvadrat.UseVisualStyleBackColor=true;
            btnDodajKvadrat.Click+=btnDodajKvadrat_Click;
            // 
            // lblStranica
            // 
            lblStranica.AutoSize=true;
            lblStranica.Location=new Point(15, 26);
            lblStranica.Name="lblStranica";
            lblStranica.Size=new Size(49, 15);
            lblStranica.TabIndex=1;
            lblStranica.Text="Stranica";
            // 
            // tbKvadratStr
            // 
            tbKvadratStr.Location=new Point(31, 53);
            tbKvadratStr.Name="tbKvadratStr";
            tbKvadratStr.Size=new Size(135, 23);
            tbKvadratStr.TabIndex=2;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(btnDodajKruznicu);
            groupBox1.Controls.Add(lblPolumjer);
            groupBox1.Controls.Add(tbKruznicaR);
            groupBox1.Location=new Point(27, 150);
            groupBox1.Name="groupBox1";
            groupBox1.Size=new Size(199, 119);
            groupBox1.TabIndex=4;
            groupBox1.TabStop=false;
            groupBox1.Text="Kruznica";
            // 
            // btnDodajKruznicu
            // 
            btnDodajKruznicu.Location=new Point(30, 82);
            btnDodajKruznicu.Name="btnDodajKruznicu";
            btnDodajKruznicu.Size=new Size(135, 23);
            btnDodajKruznicu.TabIndex=3;
            btnDodajKruznicu.Text="Dodaj kruznicu";
            btnDodajKruznicu.UseVisualStyleBackColor=true;
            btnDodajKruznicu.Click+=btnDodajKruznicu_Click;
            // 
            // lblPolumjer
            // 
            lblPolumjer.AutoSize=true;
            lblPolumjer.Location=new Point(15, 26);
            lblPolumjer.Name="lblPolumjer";
            lblPolumjer.Size=new Size(55, 15);
            lblPolumjer.TabIndex=1;
            lblPolumjer.Text="Polumjer";
            // 
            // tbKruznicaR
            // 
            tbKruznicaR.Location=new Point(30, 53);
            tbKruznicaR.Name="tbKruznicaR";
            tbKruznicaR.Size=new Size(135, 23);
            tbKruznicaR.TabIndex=2;
            // 
            // gbPravokutnik
            // 
            gbPravokutnik.Controls.Add(lblStranica2);
            gbPravokutnik.Controls.Add(tbPravokutnikStr2);
            gbPravokutnik.Controls.Add(btnDodajPravokutnik);
            gbPravokutnik.Controls.Add(lblStranica1);
            gbPravokutnik.Controls.Add(tbPravokutnikStr1);
            gbPravokutnik.ForeColor=SystemColors.ControlText;
            gbPravokutnik.Location=new Point(27, 275);
            gbPravokutnik.Name="gbPravokutnik";
            gbPravokutnik.Size=new Size(199, 163);
            gbPravokutnik.TabIndex=4;
            gbPravokutnik.TabStop=false;
            gbPravokutnik.Text="Kvadrat";
            // 
            // lblStranica2
            // 
            lblStranica2.AutoSize=true;
            lblStranica2.Location=new Point(15, 87);
            lblStranica2.Name="lblStranica2";
            lblStranica2.Size=new Size(58, 15);
            lblStranica2.TabIndex=4;
            lblStranica2.Text="Stranica 2";
            // 
            // tbPravokutnikStr2
            // 
            tbPravokutnikStr2.Location=new Point(30, 105);
            tbPravokutnikStr2.Name="tbPravokutnikStr2";
            tbPravokutnikStr2.Size=new Size(135, 23);
            tbPravokutnikStr2.TabIndex=5;
            // 
            // btnDodajPravokutnik
            // 
            btnDodajPravokutnik.Location=new Point(30, 134);
            btnDodajPravokutnik.Name="btnDodajPravokutnik";
            btnDodajPravokutnik.Size=new Size(135, 23);
            btnDodajPravokutnik.TabIndex=3;
            btnDodajPravokutnik.Text="Dodaj pravokutnik";
            btnDodajPravokutnik.UseVisualStyleBackColor=true;
            btnDodajPravokutnik.Click+=btnDodajPravokutnik_Click;
            // 
            // lblStranica1
            // 
            lblStranica1.AutoSize=true;
            lblStranica1.Location=new Point(15, 26);
            lblStranica1.Name="lblStranica1";
            lblStranica1.Size=new Size(58, 15);
            lblStranica1.TabIndex=1;
            lblStranica1.Text="Stranica 1";
            // 
            // tbPravokutnikStr1
            // 
            tbPravokutnikStr1.Location=new Point(31, 53);
            tbPravokutnikStr1.Name="tbPravokutnikStr1";
            tbPravokutnikStr1.Size=new Size(135, 23);
            tbPravokutnikStr1.TabIndex=2;
            // 
            // tbPopis
            // 
            tbPopis.Location=new Point(277, 48);
            tbPopis.Multiline=true;
            tbPopis.Name="tbPopis";
            tbPopis.Size=new Size(418, 390);
            tbPopis.TabIndex=4;
            // 
            // gbKruznica
            // 
            AutoScaleDimensions=new SizeF(7F, 15F);
            AutoScaleMode=AutoScaleMode.Font;
            ClientSize=new Size(749, 480);
            Controls.Add(tbPopis);
            Controls.Add(gbPravokutnik);
            Controls.Add(groupBox1);
            Controls.Add(gbKvadrat);
            Name="gbKruznica";
            Text="Kruznica";
            gbKvadrat.ResumeLayout(false);
            gbKvadrat.PerformLayout();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            gbPravokutnik.ResumeLayout(false);
            gbPravokutnik.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private GroupBox gbKvadrat;
        private Button btnDodajKvadrat;
        private Label lblStranica;
        private TextBox tbKvadratStr;
        private GroupBox groupBox1;
        private Button btnDodajKruznicu;
        private Label lblPolumjer;
        private TextBox tbKruznicaR;
        private GroupBox gbPravokutnik;
        private Label lblStranica2;
        private TextBox tbPravokutnikStr2;
        private Button btnDodajPravokutnik;
        private Label lblStranica1;
        private TextBox tbPravokutnikStr1;
        private TextBox tbPopis;
    }
}