﻿namespace Apstrakcija
{
    internal class Kvadrat : GeometrijskiOblik, IGeometrijskiOblik
    {
        public Kvadrat(double duljinaStranice) : base(duljinaStranice) { }
        public override double IzracunajPovrsinu()
        {
            return DuljinaStranice * DuljinaStranice;
        }
    }
}
