﻿namespace Apstrakcija
{
    internal class Kruznica : GeometrijskiOblik, IGeometrijskiOblik
    {
        private readonly double PI = 3.14159265359;

        // duljinaStranice === polumjer
        public Kruznica(double duljinaStranice) : base(duljinaStranice) { }
        public override double IzracunajPovrsinu()
        {
            return DuljinaStranice * DuljinaStranice * PI;
        }
    }
}
